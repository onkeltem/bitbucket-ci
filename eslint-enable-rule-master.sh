#!/usr/bin/env bash

[[ $1 ]] || { echo "Please specifiy some correct ESLint rules"; exit 1; }

echo "Enabling ESLint rule: $1"
git diff --name-status master.. | awk '/^A|M/ { print $2 }' | grep -E '\.(js|jsx)$' | while read -r filename; do
  echo Patching file: $filename
  sed -i -r "/^\/\* eslint-disable $1/d" $filename
done
