var request = require('supertest');
var app = require('../src/server.js');

describe('GET /', function () {
  it('displays "Hello World!"', function (done) {
    request(app).get('/').expect('Hello World!', done);
  });
  it('displays "Hello World 2!"', function (done) {
    request(app).get('/2').expect('Hello World 2!', done);
  });
});

