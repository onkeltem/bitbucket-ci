#!/usr/bin/env bash

[[ $1 ]] || { echo "Please specifiy some correct ESLint rules"; exit 1; }

echo "Disabling ESLint rule: $1"
find  ./src -type f \( -iname \*.js -o -iname \*.jsx \) -print0 | while IFS= read -r -d '' filename; do
  echo Patching file: $filename
  sed -i "1 s/^/\/\* eslint-disable $1 \*\/\n/" $filename
done
