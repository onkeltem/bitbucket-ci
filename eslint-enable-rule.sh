#!/usr/bin/env bash

[[ $1 ]] || { echo "Please specifiy some correct ESLint rules"; exit 1; }

echo "Enabling ESLint rule: $1"
find  ./src -type f \( -iname \*.js -o -iname \*.jsx \) -print0 | while IFS= read -r -d '' filename; do
  echo Patching file: $filename
  sed -i -r "/^\/\* eslint-disable $1/d" $filename
done
