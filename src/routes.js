/* eslint-disable no-unused-vars */
function routes(app) {
  app.get('/', function (req, res) {
    res.send('Hello World!')
  });

  app.get('/2', function (req, res) {
    res.send('Hello World 2!')
  });

  app.get('/exit', function () {
    process.exit(0);
  });
}

module.exports = routes;
